package com.example.user.politekneknegeripontianak;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by edwin on 04/08/2018.
 */

public class KhsList extends ArrayAdapter<Khs> {

    private Activity context;
    List<Khs> khss;

    public KhsList(Activity context, List<Khs> khss){
        super(context, R.layout.users_nilai_layout, khss);
        this.context = context;
        this.khss = khss;
    }

    @Override
    public View getView(int position,View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.users_nilai_layout,null,true);

        TextView textViewKode = (TextView) listViewItem.findViewById(R.id.user_single_name);
        TextView textViewMakul = (TextView) listViewItem.findViewById(R.id.user_single_status);
        TextView textViewNamadosen = (TextView) listViewItem.findViewById(R.id.angkamutu);
        TextView textViewJamkuliah = (TextView) listViewItem.findViewById(R.id.nilaimutu);
        TextView textViewsks = (TextView) listViewItem.findViewById(R.id.sks);
        TextView textViewamxsks = (TextView) listViewItem.findViewById(R.id.amxsks);


        Khs khs = khss.get(position);
        textViewKode.setText(khs.getKode());
        textViewMakul.setText(khs.getMakul());
        textViewNamadosen.setText(khs.getNilai());
        textViewJamkuliah.setText(khs.getAngka());
        textViewsks.setText(khs.getSks());
        textViewamxsks.setText(khs.getAmxsks());

        return listViewItem;
    }
}
