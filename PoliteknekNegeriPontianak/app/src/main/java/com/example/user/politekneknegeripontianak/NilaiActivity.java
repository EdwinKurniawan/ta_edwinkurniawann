package com.example.user.politekneknegeripontianak;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.io.Console;

import de.hdodenhof.circleimageview.CircleImageView;

public class NilaiActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private RecyclerView mNilaiList;

    private DatabaseReference mNilaiDatabase, mUserDatabase;

    private LinearLayoutManager mLayoutManager;

    private FirebaseUser mCurrentUser;

    private FirebaseAuth mAuth;

    private String mCurrent_user_id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_nilai);



      mCurrentUser = FirebaseAuth.getInstance().getCurrentUser();
        mAuth = FirebaseAuth.getInstance();

        String current_uid = mCurrentUser.getUid();

        mToolbar = (Toolbar) findViewById(R.id.users_appBar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle("Nilai");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        mNilaiDatabase = FirebaseDatabase.getInstance().getReference("Hasilstudi");

        mLayoutManager = new LinearLayoutManager(this);

        mNilaiList = (RecyclerView) findViewById(R.id.nilai_list);
        mNilaiList.setHasFixedSize(true);
        mNilaiList.setLayoutManager(mLayoutManager);


    }

    @Override
    protected void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<Nilai, NilaiViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Nilai, NilaiViewHolder>(

                Nilai.class,
                R.layout.nilai_user_layout,
                NilaiViewHolder.class,
                mNilaiDatabase

        ) {
            @Override
            protected void populateViewHolder(NilaiViewHolder nilaiViewHolder, Nilai nilai, int position) {

                nilaiViewHolder.setDisplayKode(nilai.getKode());
                nilaiViewHolder.setDisplayMakul(nilai.getMakul());
                nilaiViewHolder.setDisplayAngkamutu(nilai.getAngkamutu());



            }

        };

        mNilaiList.setAdapter(firebaseRecyclerAdapter);


    }


    public static class NilaiViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public NilaiViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }


        public void setDisplayKode(String kode){

            TextView userKodeView = (TextView) mView.findViewById(R.id.user_single_name);
            userKodeView.setText(kode);

        }

        public void setDisplayMakul(String makul){

            TextView userMakulView = (TextView) mView.findViewById(R.id.user_single_status);
            userMakulView.setText(makul);


        }

        public void setDisplayAngkamutu(String angkamutu){

            TextView userAngkamutuView = (TextView) mView.findViewById(R.id.angkamutu);
            userAngkamutuView.setText(angkamutu);


        }

/*public void setDisplayMakul1(String makul1){

    TextView userAngkamutuView = (TextView) mView.findViewById(R.id.angkamutu);
    userAngkamutuView.setText(makul1);
}
        public void setDisplayMakul2(String makul2){

            TextView userAngkamutuView = (TextView) mView.findViewById(R.id.user_single_status);
            userAngkamutuView.setText(makul2);
        }*/

    }

}
