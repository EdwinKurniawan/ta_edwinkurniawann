package com.example.user.politekneknegeripontianak;


/**
 * Created by AkshayeJH on 19/06/17.
 */

public class Khs {

    public String kode;
    public String makul;
    public String angka;
    public String nilai;
    public String sks;
    public String amxsks;




    public Khs(){

    }

    public Khs(String kode, String makul, String angka, String nilai, String sks, String amxsks) {
        this.kode = kode;
        this.makul = makul;
        this.angka = angka;
        this.nilai = nilai;
        this.sks = sks;
        this.amxsks = amxsks;

    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getMakul() {
        return makul;
    }

    public void setMakul(String makul) {
        this.makul = makul;
    }

    public String getAngka() {
        return angka;
    }

    public void setAngka(String angka) {
        this.angka = angka;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNila(String nilai) {
        this.nilai = nilai;
    }

    public String getSks() {
        return sks;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }

    public String getAmxsks() {
        return amxsks;
    }

    public void setAmxsks(String amxsks) {
        this.amxsks = amxsks;
    }

   /* public void setAngkamutu(String angkamutu) {
        this.angkamutu = angkamutu;
    }

    public String getNilaimutu() {
        return nilaimutu;
    }

    public void setNilaimutu(String nilaimutu) {
        this.nilaimutu = nilaimutu;
    }

    public String getSks(){
        return sks;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }

    public String getAmxsks() {
        return amxsks;
    }

    public void setAmxsks(String amxsks) {
        this.amxsks = amxsks;
    }*/



}
