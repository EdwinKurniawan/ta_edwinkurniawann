package com.example.user.politekneknegeripontianak;

/**
 * Created by edwin on 15/08/2018.
 */

public class Nilais {
    private String uid;
    private String title;
    private String content;

    public Nilais() {
    }

    public Nilais(String uid, String title, String content) {
        this.uid = uid;
        this.title = title;
        this.content = content;
    }

    public String getUid() {
        return uid;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }
}

