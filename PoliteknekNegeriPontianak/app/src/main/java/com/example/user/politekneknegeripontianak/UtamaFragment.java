package com.example.user.politekneknegeripontianak;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ServerValue;

/**
 * Created by edwin on 10/07/2018.
 */

public class UtamaFragment extends Fragment {


    ImageView bProfile;
    ImageView bAgenda;
    ImageView bNilai;
    ImageView bJadwal;
    ImageView bSemuaPengguna;
    ImageView bLogout;

    private RecyclerView mFriendsList;

    private DatabaseReference mFriendsDatabase;
    private DatabaseReference mUsersDatabase;

    private FirebaseAuth mAuth;

    private String mCurrent_user_id;

    private DatabaseReference mUserRef;
    private View mMainView;

    public UtamaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mMainView = inflater.inflate(R.layout.fragment_utama, container, false);


        mAuth = FirebaseAuth.getInstance();

        mCurrent_user_id = mAuth.getCurrentUser().getUid();

        if (mAuth.getCurrentUser() != null) {


            mUserRef = FirebaseDatabase.getInstance().getReference().child("Users").child(mAuth.getCurrentUser().getUid());

        }

        mFriendsDatabase = FirebaseDatabase.getInstance().getReference().child("Friends").child(mCurrent_user_id);
        mFriendsDatabase.keepSynced(true);
        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Users");
        mUsersDatabase.keepSynced(true);

        // Inflate the layout for this fragment
        return mMainView;

    }

    private void sendToStart() {

        Intent startIntent = new Intent(getContext(), StartActivity.class);
        startActivity(startIntent);

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        bProfile = (ImageView) view.findViewById(R.id.btnProfile);
        bNilai   = (ImageView) view.findViewById(R.id.btnNilai);
        bSemuaPengguna   = (ImageView) view.findViewById(R.id.ImgSemuaPengguna);
        bLogout   = (ImageView) view.findViewById(R.id.btnLogout);
        bJadwal   = (ImageView) view.findViewById(R.id.jadwal);


        bProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent profileIntent = new Intent(getContext(), SettingsActivity.class);
                startActivity(profileIntent);
            }
        });


        bNilai.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              Intent profileIntent = new Intent(getContext(),HasilStudiActivity.class);

                startActivity(profileIntent);
            }
        });

        bSemuaPengguna.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingsIntent = new Intent(getContext(), UsersActivity.class);
                startActivity(settingsIntent);
            }
        });


        bJadwal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent settingsIntent = new Intent(getContext(), JadwalActivity.class);
                startActivity(settingsIntent);
            }
        });
        bLogout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                    mUserRef.child("online").setValue(ServerValue.TIMESTAMP);

                    FirebaseAuth.getInstance().signOut();
                    sendToStart();

            }
        });
    }
}
