package com.example.user.politekneknegeripontianak;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by AkshayeJH on 11/06/17.
 */

class SectionsPagerAdapter extends FragmentPagerAdapter {


    public SectionsPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch(position) {
            case 0:
                AgendaFragment agendaFragment = new AgendaFragment();
                return agendaFragment;

            /*case 1:

                RequestsFragment requestsFragment = new RequestsFragment();
                return requestsFragment;*/

            case 1:
                ChatsFragment chatsFragment = new ChatsFragment();
                return  chatsFragment;


            case 2:
                FriendsFragment friendsFragment = new FriendsFragment();
                return friendsFragment;


            case 3:
                UtamaFragment utamaFragment = new UtamaFragment();
                return utamaFragment;

            default:
                return  null;
        }

    }

    @Override
    public int getCount() {
        return 4;
    }

    public CharSequence getPageTitle(int position){

        switch (position) {
            case 0:
                return "AGENDA";

            /*case 1:
                return "REQUES";*/


            case 1:
                return "CHATS";


            case 2:
                return "TEMAN";


            case 3:
                return "MENU";

            default:
                return null;
        }

    }

}
