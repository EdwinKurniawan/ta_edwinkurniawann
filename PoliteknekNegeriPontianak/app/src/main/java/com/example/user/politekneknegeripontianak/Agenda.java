package com.example.user.politekneknegeripontianak;

/**
 * Created by AkshayeJH on 19/06/17.
 */

public class Agenda {

    public String judul;
    public String isi;
    public String image;



    public Agenda(){

    }

    public Agenda(String judul, String isi, String image) {
        this.judul = judul;
        this.isi = isi;
        this.image = image;
    }

    public String getJudul() {
        return judul;
    }

    public void setJudul(String judul) {
        this.judul = judul;
    }

    public String getIsi() {
        return isi;
    }

    public void setIsi(String isi) {
        this.isi = isi;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
