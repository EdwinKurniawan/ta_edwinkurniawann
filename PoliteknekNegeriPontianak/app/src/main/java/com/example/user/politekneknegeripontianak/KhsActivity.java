package com.example.user.politekneknegeripontianak;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.widget.ListView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class KhsActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private RecyclerView mUsersList;

    private DatabaseReference mUsersDatabase;

    private FirebaseAuth mAuth;
    private LinearLayoutManager mLayoutManager;

    ListView listViewNilai;

    List<Khs> khss;

    private DatabaseReference mUserRef;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_studi);

       /* mToolbar = (Toolbar) findViewById(R.id.users_appBar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle("All Users");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


*/
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser fUsers = FirebaseAuth.getInstance().getCurrentUser();
        mUsersDatabase = FirebaseDatabase.getInstance().getReference("Hasil");


        listViewNilai = (ListView) findViewById(R.id.listViewNilai);

        khss = new ArrayList<>();
/*
        if (mAuth.getCurrentUser() != null) {


            mUserRef = FirebaseDatabase.getInstance().getReference().child("Hasil").child(mAuth.getCurrentUser().getUid());
        }*/

     /*   mLayoutManager = new LinearLayoutManager(this);

        mUsersList = (RecyclerView) findViewById(R.id.hasilstudi_list);
        mUsersList.setHasFixedSize(true);
        mUsersList.setLayoutManager(mLayoutManager);
*/

    }

    @Override
    protected void onStart() {
        super.onStart();


        mUsersDatabase.addListenerForSingleValueEvent(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot khsSnapshot : dataSnapshot.getChildren()) {
                    Khs khs = khsSnapshot.getValue(Khs.class);
                    khss.add(khs);

                }

                KhsList adapter = new KhsList(KhsActivity.this, khss);
                listViewNilai.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

       /* FirebaseRecyclerAdapter<Hasil, HasilstudiViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Hasil, HasilstudiViewHolder>(

                Hasil.class,
                R.layout.users_nilai_layout,
                HasilstudiViewHolder.class,
                mUsersDatabase

        ) {
            @Override
            protected void populateViewHolder(HasilstudiViewHolder hasilstudiViewHolder, Hasil hasilstudi, int position) {

                hasilstudiViewHolder.setDisplayKode(hasilstudi.getKode());
                hasilstudiViewHolder.setUserMakul(hasilstudi.getMakul());
                hasilstudiViewHolder.setUserAngkamutu(hasilstudi.getAngkamutu());



            }
        };


        mUsersList.setAdapter(firebaseRecyclerAdapter);
*/
    }

/*
    public static class HasilstudiViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public HasilstudiViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setDisplayKode(String kode){

            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(kode);

        }

        public void setUserMakul(String makul){

            TextView userStatusView = (TextView) mView.findViewById(R.id.user_single_status);
            userStatusView.setText(makul);


        }

        public void setUserAngkamutu(String angkamutu){

            TextView userStatusView = (TextView) mView.findViewById(R.id.angkamutu);
            userStatusView.setText(angkamutu);


        }*/

       /* public void setUserImage(String thumb_image, Context ctx){

            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);

            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.default_avatar).into(userImageView);

        }*/



}
