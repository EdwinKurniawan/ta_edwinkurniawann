package com.example.user.politekneknegeripontianak;


/**
 * Created by AkshayeJH on 19/06/17.
 */

public class Jadwal {

    public String kode;
    public String makul;
    public String namadosen;
    public String jamkuliah;
    public String ruangan;




    public Jadwal(){

    }

    public Jadwal(String kode, String makul, String namadosen, String jamkuliah, String ruangan) {
        this.kode = kode;
        this.makul = makul;
        this.namadosen = namadosen;
        this.jamkuliah = jamkuliah;
        this.ruangan = ruangan;

    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getMakul() {
        return makul;
    }

    public void setMakul(String makul) {
        this.makul = makul;
    }

    public String getNamadosen() {
        return namadosen;
    }

    public void setNamadosen(String namadosen) {
        this.namadosen = namadosen;
    }

    public String getJamkuliah() {
        return jamkuliah;
    }

    public void setJamkuliah(String jamkuliah) {
        this.jamkuliah = jamkuliah;
    }

    public String getRuangan() {
        return ruangan;
    }

    public void setRuangan(String ruangan) {
        this.ruangan = ruangan;
    }

    /* public void setAngkamutu(String angkamutu) {
        this.angkamutu = angkamutu;
    }

    public String getNilaimutu() {
        return nilaimutu;
    }

    public void setNilaimutu(String nilaimutu) {
        this.nilaimutu = nilaimutu;
    }

    public String getSks(){
        return sks;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }

    public String getAmxsks() {
        return amxsks;
    }

    public void setAmxsks(String amxsks) {
        this.amxsks = amxsks;
    }*/



}
