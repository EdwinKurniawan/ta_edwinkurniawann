package com.example.user.politekneknegeripontianak;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;


public class AgendaFragment extends Fragment {

    private RecyclerView mFriendsList;

    private DatabaseReference mFriendsDatabase;
    private DatabaseReference mUsersDatabase;

    private FirebaseAuth mAuth;

    private String mCurrent_user_id;

    private View mMainView;

    public AgendaFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        mMainView = inflater.inflate(R.layout.fragment_agenda, container, false);

        mFriendsList = (RecyclerView) mMainView.findViewById(R.id.agenda_list);

        mUsersDatabase = FirebaseDatabase.getInstance().getReference().child("Agenda");
        mUsersDatabase.keepSynced(true);


        mFriendsList.setHasFixedSize(true);
        mFriendsList.setLayoutManager(new LinearLayoutManager(getContext()));

        // Inflate the layout for this fragment
        return mMainView;
    }

    @Override
    public void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<Agenda,UsersViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Agenda, UsersViewHolder>(

                Agenda.class,
                R.layout.agenda_single_layout,
                UsersViewHolder.class,
                mUsersDatabase

        ) {
            @Override
            protected void populateViewHolder(UsersViewHolder usersViewHolder, Agenda users, int position) {

                usersViewHolder.setDisplayName(users.getJudul());
                /*usersViewHolder.setUserStatus(users.getIsi());*/
                usersViewHolder.setUserImage(users.getImage(), getContext());

                final String agenda_id = getRef(position).getKey();

                usersViewHolder.mView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        Intent intent = new Intent(getContext(), DetailAgendaActivity.class);
                        intent.putExtra("agenda_id", agenda_id);
                        startActivity(intent);

                    }
                });


            }
        };


        mFriendsList.setAdapter(firebaseRecyclerAdapter);

    }


    public static class UsersViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public UsersViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setDisplayName(String judul){

            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(judul);

        }

      /*  public void setUserStatus(String isi){

            TextView userStatusView = (TextView) mView.findViewById(R.id.user_single_status);
            userStatusView.setText(isi);


        }*/

        public void setUserImage(String image, Context ctx){

            ImageView userImageView = (ImageView) mView.findViewById(R.id.user_single_image);

            Picasso.with(ctx).load(image).placeholder(R.drawable.profile).into(userImageView);

        }


    }

   /* @Override
    public void onStart() {
        super.onStart();

        FirebaseRecyclerAdapter<Agenda, AgendaViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Agenda, AgendaViewHolder>(

                Agenda.class,
                R.layout.agenda_single_layout,
                AgendaViewHolder.class,
                mUsersDatabase

        ) {
            @Override
            protected void populateViewHolder(AgendaViewHolder agendaViewHolder, Agenda agenda, int position) {




                agendaViewHolder.setDisplayName(agenda.getJudul());
                agendaViewHolder.setUserStatus(agenda.getIsi());
                agendaViewHolder.setUserImage(agenda.getImage(), getContext());



            }
        };


        mFriendsList.setAdapter(firebaseRecyclerAdapter);

    }


    public static class AgendaViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public AgendaViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setDisplayName(String judul){

            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(judul);

        }

        public void setUserStatus(String isi){

            TextView userStatusView = (TextView) mView.findViewById(R.id.user_single_status);
            userStatusView.setText(isi);


        }

        public void setUserImage(String image, Context ctx){

            ImageView userImageView = (ImageView) mView.findViewById(R.id.user_single_image);

            Picasso.with(ctx).load(image).placeholder(R.drawable.profile).into(userImageView);


        }


    }
*/

}
