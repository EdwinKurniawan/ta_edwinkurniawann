package com.example.user.politekneknegeripontianak;

public class Hasil{

    public String kode;
    public String makul;
    public String angkamutu;
    public String nilaimutu;
    public String sks;
    public String amxsks;

    public Hasil(){

    }

    public Hasil(String kode, String makul, String angkamutu, String nilaimutu, String sks, String amxsks){

        this.kode = kode;
        this.makul = makul;
        this.angkamutu = angkamutu;
        this.nilaimutu = nilaimutu;
        this.sks = sks;
        this.amxsks = amxsks;

    }

    public String getKode() {
        return kode;
    }

    public void setKode(String kode) {
        this.kode = kode;
    }

    public String getMakul() {
        return makul;
    }

    public void setMakul(String makul) {
        this.makul = makul;
    }

    public String getAngkamutu() {
        return angkamutu;
    }

    public void setAngkamutu(String angkamutu) {
        this.angkamutu = angkamutu;
    }

    public String getNilaimutu() {
        return nilaimutu;
    }

    public void setNilaimutu(String nilaimutu) {
        this.nilaimutu = nilaimutu;
    }

    public String getSks() {
        return sks;
    }

    public void setSks(String sks) {
        this.sks = sks;
    }

    public String getAmxsks() {
        return amxsks;
    }

    public void setAmxsks(String amxsks) {
        this.amxsks = amxsks;
    }
}
