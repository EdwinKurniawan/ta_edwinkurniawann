package com.example.user.politekneknegeripontianak;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

public class HasilStudiActivity extends AppCompatActivity {

    private Toolbar mToolbar;

    private RecyclerView mUsersList;

    private DatabaseReference mUsersDatabase;

    private FirebaseAuth mAuth;
    private LinearLayoutManager mLayoutManager;

    ListView listViewNilai;

    List<Hasil> hasils;

    private DatabaseReference mUserRef;
    private String mCurrentUserId;
    private DatabaseReference semester;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hasil_studi);

       /* mToolbar = (Toolbar) findViewById(R.id.users_appBar);
        setSupportActionBar(mToolbar);

        getSupportActionBar().setTitle("All Users");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);


*/

        mAuth = FirebaseAuth.getInstance();
        mCurrentUserId = mAuth.getCurrentUser().getUid();
        mUsersDatabase = FirebaseDatabase.getInstance().getReference("Hasil");
        semester = mUsersDatabase.child(mCurrentUserId);

        listViewNilai = (ListView) findViewById(R.id.listViewNilai);

        hasils = new ArrayList<>();
/*
        if (mAuth.getCurrentUser() != null) {


            mUserRef = FirebaseDatabase.getInstance().getReference().child("Hasil").child(mAuth.getCurrentUser().getUid());
        }*/

     /*   mLayoutManager = new LinearLayoutManager(this);

        mUsersList = (RecyclerView) findViewById(R.id.hasilstudi_list);
        mUsersList.setHasFixedSize(true);
        mUsersList.setLayoutManager(mLayoutManager);
*/

    }

    @Override
    protected void onStart() {
        super.onStart();

        mUsersDatabase.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot) {


                for (DataSnapshot hasilstudiSnapshot : dataSnapshot.getChildren()) {
                    Hasil hasilstudi = hasilstudiSnapshot.getValue(Hasil.class);
                    hasils.add(hasilstudi);

                }

                HasilstudiList adapter = new HasilstudiList( HasilStudiActivity.this, hasils);
                listViewNilai.setAdapter(adapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError) {

            }
        });

       /* FirebaseRecyclerAdapter<Hasil, HasilstudiViewHolder> firebaseRecyclerAdapter = new FirebaseRecyclerAdapter<Hasil, HasilstudiViewHolder>(

                Hasil.class,
                R.layout.users_nilai_layout,
                HasilstudiViewHolder.class,
                mUsersDatabase

        ) {
            @Override
            protected void populateViewHolder(HasilstudiViewHolder hasilstudiViewHolder, Hasil hasilstudi, int position) {

                hasilstudiViewHolder.setDisplayKode(hasilstudi.getKode());
                hasilstudiViewHolder.setUserMakul(hasilstudi.getMakul());
                hasilstudiViewHolder.setUserAngkamutu(hasilstudi.getAngkamutu());



            }
        };


        mUsersList.setAdapter(firebaseRecyclerAdapter);
*/
    }

/*
    public static class HasilstudiViewHolder extends RecyclerView.ViewHolder {

        View mView;

        public HasilstudiViewHolder(View itemView) {
            super(itemView);

            mView = itemView;

        }

        public void setDisplayKode(String kode){

            TextView userNameView = (TextView) mView.findViewById(R.id.user_single_name);
            userNameView.setText(kode);

        }

        public void setUserMakul(String makul){

            TextView userStatusView = (TextView) mView.findViewById(R.id.user_single_status);
            userStatusView.setText(makul);


        }

        public void setUserAngkamutu(String angkamutu){

            TextView userStatusView = (TextView) mView.findViewById(R.id.angkamutu);
            userStatusView.setText(angkamutu);


        }*/

       /* public void setUserImage(String thumb_image, Context ctx){

            CircleImageView userImageView = (CircleImageView) mView.findViewById(R.id.user_single_image);

            Picasso.with(ctx).load(thumb_image).placeholder(R.drawable.default_avatar).into(userImageView);

        }*/



}
