package com.example.user.politekneknegeripontianak;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by edwin on 04/08/2018.
 */

public class JadwalList extends ArrayAdapter<Jadwal> {

    private Activity context;
    List<Jadwal> jadwals;

    public JadwalList(Activity context, List<Jadwal> jadwals){
        super(context, R.layout.users_jadwal_layout, jadwals);
        this.context = context;
        this.jadwals = jadwals;
    }

    @Override
    public View getView(int position,View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.users_jadwal_layout,null,true);

        TextView textViewKode = (TextView) listViewItem.findViewById(R.id.user_single_name);
        TextView textViewMakul = (TextView) listViewItem.findViewById(R.id.user_single_status);
        TextView textViewNamadosen = (TextView) listViewItem.findViewById(R.id.angkamutu);
        TextView textViewJamkuliah = (TextView) listViewItem.findViewById(R.id.nilaimutu);
        TextView textViewRuangan = (TextView) listViewItem.findViewById(R.id.ruangan);


        Jadwal jadwal = jadwals.get(position);
        textViewKode.setText(jadwal.getKode());
        textViewMakul.setText(jadwal.getMakul());
        textViewNamadosen.setText(jadwal.getNamadosen());
        textViewJamkuliah.setText(jadwal.getJamkuliah());
        textViewRuangan.setText(jadwal.getRuangan());

        return listViewItem;
    }
}
