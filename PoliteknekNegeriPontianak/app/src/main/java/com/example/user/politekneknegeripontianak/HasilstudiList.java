package com.example.user.politekneknegeripontianak;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

/**
 * Created by edwin on 04/08/2018.
 */

public class HasilstudiList extends ArrayAdapter<Hasil> {

    private Activity context;
    List<Hasil> hasils;

    public HasilstudiList(Activity context, List<Hasil> hasils){
        super(context, R.layout.users_nilai_layout, hasils);
        this.context = context;
        this.hasils = hasils;
    }

    @Override
    public View getView(int position,View convertView, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        View listViewItem = inflater.inflate(R.layout.users_nilai_layout,null,true);

        TextView textViewKode = (TextView) listViewItem.findViewById(R.id.user_single_name);
        TextView textViewMakul = (TextView) listViewItem.findViewById(R.id.user_single_status);
        TextView textViewAngkamutu = (TextView) listViewItem.findViewById(R.id.angkamutu);
        TextView textViewNilaimutu = (TextView) listViewItem.findViewById(R.id.nilaimutu);
        TextView textViewSks = (TextView) listViewItem.findViewById(R.id.sks);
        TextView textViewAmxsks = (TextView) listViewItem.findViewById(R.id.amxsks);

        Hasil hasil = hasils.get(position);
        textViewKode.setText(hasil.getKode());
        textViewMakul.setText(hasil.getMakul());
        textViewAngkamutu.setText(hasil.getAngkamutu());
        textViewNilaimutu.setText(hasil.getNilaimutu());
        textViewSks.setText(hasil.getSks());
        textViewAmxsks.setText(hasil.getAmxsks());

        return listViewItem;
    }
}
